local json = require("dkjson")
local http = require("socket.http")
local ltn12 = require("ltn12")

local token_file = assert(io.open("token","r"))
local token = token_file:read("*all")
token_file:close()

local url = string.gsub("http://192.168.1.15:8081/bot" .. token .. "/sendMessage", "\n", "")
local arg = {...}
local json_input = json.decode(string.gsub(arg[1], "\n", ""))

function http_post (json_reply)
	http.request {
		url = url,
		method = 'POST',
		headers = {
			["Content-Type"] = "application/json",
			["Content-Length"] = string.len(json_reply)
		},
		source = ltn12.source.string(json_reply)
	}
end

function send_message (chat_id, text)
	local body = {
		chat_id = chat_id,
		text = text
	}
	http_post(json.encode(body))
end

local user_text = json_input.message.text

if user_text == "test" then
	local chat_id = json_input.message.chat.id
	send_message(chat_id, user_text)
end
